#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 11

#define LIST_SIZE 4096

int list_sort(const void *first, const void *second) { return *((int *)first) < *((int *)second) ? -1 : 1; }

int find_seat(const int list[LIST_SIZE], const size_t list_size) {
  int i = list_size / 2;

  int j = list_size / 2;

  for (; i >= 0 || (size_t)j < list_size; --i, ++j) {
    if (i >= 0 && AOC_ABS(list[i] - list[i + 1]) > 1) {
      return list[i] + 1;
    }

    if ((size_t)j < list_size && AOC_ABS(list[j - 1] - list[j]) > 1) {
      return list[j - 1] + 1;
    }
  }

  return -1;
}

int main(void) {
  char buffer[BUFFER_SIZE];

  int list[LIST_SIZE];

  size_t list_size = 0;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%s", buffer) != EOF) {
    size_t index = 0;

    int lower = 0;

    int upper = 127;

    int row = 0;

    int column = 0;

    for (; index < BUFFER_SIZE - 4; ++index) {
      if (buffer[index] == 'F') {
        upper = (lower + upper + 1) / 2 - 1;

        continue;
      }

      lower = (lower + upper + 1) / 2;
    }

    row = lower;

    lower = 0;

    upper = 7;

    for (; index < BUFFER_SIZE; ++index) {
      if (buffer[index] == 'L') {
        upper = (lower + upper + 1) / 2 - 1;

        continue;
      }

      lower = (lower + upper + 1) / 2;
    }

    column = lower;

    list[list_size] = (row * 8) + column;

    ++list_size;
  }

  qsort(list, list_size, sizeof(int), list_sort);

  fprintf(stdout, "%d\n", find_seat(list, list_size));

  return EXIT_SUCCESS;
}
