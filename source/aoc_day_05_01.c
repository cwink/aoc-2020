#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 11

int main(void) {
  char buffer[BUFFER_SIZE];

  int highest = 0;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%s", buffer) != EOF) {
    size_t index = 0;

    int lower = 0;

    int upper = 127;

    int row = 0;

    int column = 0;

    int result = 0;

    for (; index < BUFFER_SIZE - 4; ++index) {
      if (buffer[index] == 'F') {
        upper = (lower + upper + 1) / 2 - 1;

        continue;
      }

      lower = (lower + upper + 1) / 2;
    }

    row = lower;

    lower = 0;

    upper = 7;

    for (; index < BUFFER_SIZE; ++index) {
      if (buffer[index] == 'L') {
        upper = (lower + upper + 1) / 2 - 1;

        continue;
      }

      lower = (lower + upper + 1) / 2;
    }

    column = lower;

    result = (row * 8) + column;

    if (highest < result) {
      highest = result;
    }
  }

  fprintf(stdout, "%d\n", highest);

  return EXIT_SUCCESS;
}
