#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>

#define PASSWORD_MAX_SIZE 128

int main(void) {
  size_t valid_passwords = 0;

  int first = 0;

  int second = 0;

  char ch = '\0';

  char password[PASSWORD_MAX_SIZE];

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%d-%d %c: %s\n", &first, &second, &ch, password) != EOF) {
    if ((password[first - 1] == ch && password[second - 1] != ch) || (password[first - 1] != ch && password[second - 1] == ch)) {
      ++valid_passwords;
    }
  }

  fprintf(stdout, "%ld\n", valid_passwords);

  return EXIT_SUCCESS;
}
