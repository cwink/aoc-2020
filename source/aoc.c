#include "../include/aoc.h"

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdio.h>

int aoc_is_stdin_valid(void) {
  fd_set read_fds;

  struct timeval timeout;

  FD_ZERO(&read_fds);

  FD_SET(STDIN_FILENO, &read_fds);

  timeout.tv_sec = 0;

  timeout.tv_usec = 0;

  return select(1, &read_fds, NULL, NULL, &timeout) <= 0 ? 0 : 1;
}

int aoc_round(const float value) { return (int)((value - (float)((int)value)) * 10.0F) >= 5 ? (int)value + 1 : (int)value; }
