#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 128

typedef struct passport_t {
  int byr;

  int iyr;

  int eyr;

  int hgt;

  int hcl;

  int ecl;

  int pid;

  int cid;
} PASSPORT;

int main(void) {
  char buffer[BUFFER_SIZE];

  PASSPORT passport = {0, 0, 0, 0, 0, 0, 0, 0};

  size_t valid_passports = 0;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%s", buffer) != EOF) {
    char ch = '\0';

    char *key = NULL;

    key = strtok(buffer, ":");

    if (strcmp(key, "byr") == 0) {
      passport.byr = 1;
    } else if (strcmp(key, "iyr") == 0) {
      passport.iyr = 1;
    } else if (strcmp(key, "eyr") == 0) {
      passport.eyr = 1;
    } else if (strcmp(key, "hgt") == 0) {
      passport.hgt = 1;
    } else if (strcmp(key, "hcl") == 0) {
      passport.hcl = 1;
    } else if (strcmp(key, "ecl") == 0) {
      passport.ecl = 1;
    } else if (strcmp(key, "pid") == 0) {
      passport.pid = 1;
    } else if (strcmp(key, "cid") == 0) {
      passport.cid = 1;
    }

    fgetc(stdin);

    ch = fgetc(stdin);

    if (ch == '\n' || ch == EOF) {
      if (passport.byr == 1 && passport.iyr == 1 && passport.eyr == 1 && passport.hgt == 1 && passport.hcl == 1 && passport.ecl == 1 && passport.pid == 1) {
        ++valid_passports;
      }

      passport.byr = 0;

      passport.iyr = 0;

      passport.eyr = 0;

      passport.hgt = 0;

      passport.hcl = 0;

      passport.ecl = 0;

      passport.pid = 0;

      passport.cid = 0;

      continue;
    }

    ungetc(ch, stdin);
  }

  fprintf(stdout, "%ld\n", valid_passports);

  return EXIT_SUCCESS;
}
