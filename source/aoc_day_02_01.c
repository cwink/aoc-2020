#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>

#define PASSWORD_MAX_SIZE 128

int main(void) {
  size_t valid_passwords = 0;

  int min = 0;

  int max = 0;

  char ch = '\0';

  char password[PASSWORD_MAX_SIZE];

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%d-%d %c: %s\n", &min, &max, &ch, password) != EOF) {
    size_t count = 0;

    size_t i = 0;

    for (; password[i] != 0; ++i) {
      if (password[i] == ch) {
        ++count;
      }
    }

    if (count >= (size_t)min && count <= (size_t)max) {
      ++valid_passwords;
    }
  }

  fprintf(stdout, "%ld\n", valid_passwords);

  return EXIT_SUCCESS;
}
