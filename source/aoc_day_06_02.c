#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE 256

int main(void) {
  char ch = '\0';

  int table[TABLE_SIZE];

  size_t total = 0;

  size_t people = 0;

  memset(table, 0, sizeof(int) * TABLE_SIZE);

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while ((ch = fgetc(stdin)) != EOF) {
    if (ch == '\n') {
      ch = fgetc(stdin);

      ++people;

      if (ch == '\n' || ch == EOF) {
        size_t index = 0;

        for (; index < TABLE_SIZE; ++index) {
          if ((size_t)table[(size_t)index] == people) {
            ++total;
          }
        }

        memset(table, 0, sizeof(int) * TABLE_SIZE);

        people = 0;
      } else {
        ungetc(ch, stdin);
      }

      continue;
    }

    ++table[(size_t)ch];
  }

  fprintf(stdout, "%ld\n", total);

  return EXIT_SUCCESS;
}
