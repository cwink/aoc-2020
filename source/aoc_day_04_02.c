#include "../include/aoc.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 128

#define KEY_SIZE 128

#define VALUE_SIZE 128

#define BYR_MIN 1920

#define BYR_MAX 2002

#define IYR_MIN 2010

#define IYR_MAX 2020

#define EYR_MIN 2020

#define EYR_MAX 2030

#define HGT_CM_MIN 150

#define HGT_CM_MAX 193

#define HGT_IN_MIN 59

#define HGT_IN_MAX 76

#define HCL_LENGTH 6

#define HCL_NUM_MIN '0'

#define HCL_NUM_MAX '9'

#define HCL_ALPHA_MIN 'a'

#define HCL_ALPHA_MAX 'f'

#define ECL_COLORS "amb", "blu", "brn", "gry", "grn", "hzl", "oth"

#define ECL_COLORS_SIZE 7

#define ECL_COLORS_LENGTH 4

#define PID_SIZE 9

typedef struct passport_t {
  int byr;

  int iyr;

  int eyr;

  int hgt;

  int hcl;

  int ecl;

  int pid;

  int cid;
} PASSPORT;

int main(void) {
  char buffer[BUFFER_SIZE];

  PASSPORT passport = {0, 0, 0, 0, 0, 0, 0, 0};

  char ecl_colors[ECL_COLORS_SIZE][ECL_COLORS_LENGTH] = {ECL_COLORS};

  size_t valid_passports = 0;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%s", buffer) != EOF) {
    char ch = '\0';

    char *ptr = NULL;

    char key[KEY_SIZE];

    char value[VALUE_SIZE];

    ptr = strtok(buffer, ":");

    strcpy(key, ptr);

    ptr = strtok(NULL, ":");

    strcpy(value, ptr);

    if (strcmp(key, "byr") == 0) {
      int val = atoi(value);

      if (val >= BYR_MIN && val <= BYR_MAX) {
        passport.byr = 1;
      }
    } else if (strcmp(key, "iyr") == 0) {
      int val = atoi(value);

      if (val >= IYR_MIN && val <= IYR_MAX) {
        passport.iyr = 1;
      }
    } else if (strcmp(key, "eyr") == 0) {
      int val = atoi(value);

      if (val >= EYR_MIN && val <= EYR_MAX) {
        passport.eyr = 1;
      }
    } else if (strcmp(key, "hgt") == 0) {
      int val = 0;

      char type[3];

      strcpy(type, value + strlen(value) - 2);

      ptr = strtok(value, type);

      if (ptr != NULL) {
        val = atoi(ptr);

        if ((strcmp(type, "cm") == 0 && val >= HGT_CM_MIN && val <= HGT_CM_MAX) || (strcmp(type, "in") == 0 && val >= HGT_IN_MIN && val <= HGT_IN_MAX)) {
          passport.hgt = 1;
        }
      }
    } else if (strcmp(key, "hcl") == 0) {
      if (value[0] == '#') {
        size_t index = 1;

        passport.hcl = 1;

        for (; index < HCL_LENGTH; ++index) {
          if (!((value[index] >= HCL_NUM_MIN && value[index] <= HCL_NUM_MAX) || (value[index] >= HCL_ALPHA_MIN && value[index] <= HCL_ALPHA_MAX))) {
            passport.hcl = 0;

            break;
          }
        }
      }
    } else if (strcmp(key, "ecl") == 0) {
      size_t index = 0;

      for (; index < ECL_COLORS_SIZE; ++index) {
        if (strcmp(value, ecl_colors[index]) == 0) {
          passport.ecl = 1;

          break;
        }
      }
    } else if (strcmp(key, "pid") == 0) {
      size_t index = 0;

      size_t count = 0;

      for (; index < strlen(value); ++index) {
        if (isdigit(value[index]) != 0) {
          ++count;
        }
      }

      if (count == PID_SIZE) {
        passport.pid = 1;
      }
    } else if (strcmp(key, "cid") == 0) {
      passport.cid = 1;
    }

    fgetc(stdin);

    ch = fgetc(stdin);

    if (ch == '\n' || ch == EOF) {
      if (passport.byr == 1 && passport.iyr == 1 && passport.eyr == 1 && passport.hgt == 1 && passport.hcl == 1 && passport.ecl == 1 && passport.pid == 1) {
        ++valid_passports;
      }

      passport.byr = 0;

      passport.iyr = 0;

      passport.eyr = 0;

      passport.hgt = 0;

      passport.hcl = 0;

      passport.ecl = 0;

      passport.pid = 0;

      passport.cid = 0;

      continue;
    }

    ungetc(ch, stdin);
  }

  fprintf(stdout, "%ld\n", valid_passports);

  return EXIT_SUCCESS;
}
