#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 32

#define TABLE_SIZE 2048

#define BAG_SIZE 16

static unsigned int hash_first(const char *const key) {
  if (strcmp(key, "bright") == 0) {
    return 0;
  }

  if (strcmp(key, "clear") == 0) {
    return 1;
  }

  if (strcmp(key, "dark") == 0) {
    return 2;
  }

  if (strcmp(key, "dim") == 0) {
    return 3;
  }

  if (strcmp(key, "dotted") == 0) {
    return 4;
  }

  if (strcmp(key, "drab") == 0) {
    return 5;
  }

  if (strcmp(key, "dull") == 0) {
    return 6;
  }

  if (strcmp(key, "faded") == 0) {
    return 7;
  }

  if (strcmp(key, "light") == 0) {
    return 8;
  }

  if (strcmp(key, "mirrored") == 0) {
    return 9;
  }

  if (strcmp(key, "muted") == 0) {
    return 10;
  }

  if (strcmp(key, "pale") == 0) {
    return 11;
  }

  if (strcmp(key, "plaid") == 0) {
    return 12;
  }

  if (strcmp(key, "posh") == 0) {
    return 13;
  }

  if (strcmp(key, "shiny") == 0) {
    return 14;
  }

  if (strcmp(key, "striped") == 0) {
    return 15;
  }

  if (strcmp(key, "vibrant") == 0) {
    return 16;
  }

  if (strcmp(key, "wavy") == 0) {
    return 17;
  }

  return 0;
}

static unsigned int hash_second(const char *const key) {
  if (strcmp(key, "aqua") == 0) {
    return 0;
  }

  if (strcmp(key, "beige") == 0) {
    return 1;
  }

  if (strcmp(key, "black") == 0) {
    return 2;
  }

  if (strcmp(key, "blue") == 0) {
    return 3;
  }

  if (strcmp(key, "bronze") == 0) {
    return 4;
  }

  if (strcmp(key, "brown") == 0) {
    return 5;
  }

  if (strcmp(key, "chartreuse") == 0) {
    return 6;
  }

  if (strcmp(key, "coral") == 0) {
    return 7;
  }

  if (strcmp(key, "crimson") == 0) {
    return 8;
  }

  if (strcmp(key, "cyan") == 0) {
    return 9;
  }

  if (strcmp(key, "fuchsia") == 0) {
    return 10;
  }

  if (strcmp(key, "gold") == 0) {
    return 11;
  }

  if (strcmp(key, "gray") == 0) {
    return 12;
  }

  if (strcmp(key, "green") == 0) {
    return 13;
  }

  if (strcmp(key, "indigo") == 0) {
    return 14;
  }

  if (strcmp(key, "lavender") == 0) {
    return 15;
  }

  if (strcmp(key, "lime") == 0) {
    return 16;
  }

  if (strcmp(key, "magenta") == 0) {
    return 17;
  }

  if (strcmp(key, "maroon") == 0) {
    return 18;
  }

  if (strcmp(key, "olive") == 0) {
    return 19;
  }

  if (strcmp(key, "orange") == 0) {
    return 20;
  }

  if (strcmp(key, "plum") == 0) {
    return 21;
  }

  if (strcmp(key, "purple") == 0) {
    return 22;
  }

  if (strcmp(key, "red") == 0) {
    return 23;
  }

  if (strcmp(key, "salmon") == 0) {
    return 24;
  }

  if (strcmp(key, "silver") == 0) {
    return 25;
  }

  if (strcmp(key, "tan") == 0) {
    return 26;
  }

  if (strcmp(key, "teal") == 0) {
    return 27;
  }

  if (strcmp(key, "tomato") == 0) {
    return 28;
  }

  if (strcmp(key, "turquoise") == 0) {
    return 29;
  }

  if (strcmp(key, "violet") == 0) {
    return 30;
  }

  if (strcmp(key, "white") == 0) {
    return 31;
  }

  if (strcmp(key, "yellow") == 0) {
    return 32;
  }

  return 0;
}

typedef struct pair_t {
  int bags[BAG_SIZE];

  int count[BAG_SIZE];
} PAIR;

static unsigned int hash(const char *const first, const char *const second) {
  unsigned int first_num = hash_first(first);

  unsigned int second_num = hash_second(second);

  return first_num * 100 + second_num;
}

static void table_init(PAIR table[TABLE_SIZE]) {
  size_t index = 0;

  for (; index < TABLE_SIZE; ++index) {
    size_t jdex = 0;

    for (; jdex < BAG_SIZE; ++jdex) {
      table[index].bags[jdex] = -1;

      table[index].count[jdex] = -1;
    }
  }
}

static void table_bag_insert(PAIR table[TABLE_SIZE], const char *const first, const char *const second, const char *const sub_first,
                             const char *const sub_second, const int count) {
  size_t index = 0;

  unsigned int hash_1 = hash(first, second);

  unsigned int hash_2 = hash(sub_first, sub_second);

  for (; index < BAG_SIZE; ++index) {
    if (table[hash_1].bags[index] == -1) {
      table[hash_1].bags[index] = hash_2;

      table[hash_1].count[index] = count;

      break;
    }
  }
}

static unsigned int table_bag_specific_count(PAIR table[TABLE_SIZE], const unsigned int hash_1) {
  size_t index = 0;

  unsigned int count = 0;

  for (; index < BAG_SIZE; ++index) {
    if (table[hash_1].bags[index] == -1) {
      break;
    }

    count += table[hash_1].count[index];

    count += table_bag_specific_count(table, table[hash_1].bags[index]) * table[hash_1].count[index];
  }

  return count;
}

static unsigned int table_bag_count(PAIR table[TABLE_SIZE], const char *const first, const char *const second) {
  return table_bag_specific_count(table, hash(first, second));
}

int main(void) {
  PAIR table[TABLE_SIZE];

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  table_init(table);

  for (;;) {
    int result = 0;

    char first[BUFFER_SIZE];

    char second[BUFFER_SIZE];

    result = fscanf(stdin, "%s %s bags contain", first, second);

    if (result == EOF) {
      break;
    }

    for (;;) {
      char contains[BUFFER_SIZE];

      char sub_first[BUFFER_SIZE];

      char sub_second[BUFFER_SIZE];

      char ending[BUFFER_SIZE];

      int count = 0;

      result = fscanf(stdin, "%s %s %s", contains, sub_first, sub_second);

      if (result == EOF || strcmp(contains, "no") == 0) {
        break;
      }

      result = fscanf(stdin, "%s", ending);

      count = atoi(contains);

      table_bag_insert(table, first, second, sub_first, sub_second, count);

      if (result == EOF || ending[strlen(ending) - 1] != ',') {
        break;
      }
    }
  }

  fprintf(stdout, "%u\n", table_bag_count(table, "shiny", "gold"));

  return EXIT_SUCCESS;
}
