#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROW_MAX 2048

#define COL_MAX 128

#define OPEN_SQUARE '.'

#define TREE '#'

#define SLOPE_X 3

#define SLOPE_Y 1

int main(void) {
  char graph[ROW_MAX][COL_MAX];

  size_t height = 0;

  size_t width = 0;

  size_t y = 0;

  size_t x = 0;

  size_t tree_count = 0;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%s\n", graph[height]) != EOF) {
    ++height;
  }

  width = strlen(graph[0]);

  for (;;) {
    if (y >= height) {
      break;
    }

    if (graph[y][x] == TREE) {
      ++tree_count;
    }

    y += SLOPE_Y;

    x += SLOPE_X;

    if (x >= width) {
      x = x - width;
    }
  }

  fprintf(stdout, "%ld\n", tree_count);

  return EXIT_SUCCESS;
}
