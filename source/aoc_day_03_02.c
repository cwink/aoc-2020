#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROW_MAX 2048

#define COL_MAX 128

#define OPEN_SQUARE '.'

#define TREE '#'

#define SLOPE_SIZE 5

typedef struct slope_t {
  int x;

  int y;
} SLOPE;

int main(void) {
  char graph[ROW_MAX][COL_MAX];

  size_t height = 0;

  size_t width = 0;

  size_t tree_result = 1;

  size_t index = 0;

  SLOPE slope[SLOPE_SIZE] = {{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}};

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%s\n", graph[height]) != EOF) {
    ++height;
  }

  width = strlen(graph[0]);

  for (; index < SLOPE_SIZE; ++index) {
    size_t y = 0;

    size_t x = 0;

    size_t tree_count = 0;

    for (;;) {
      if (y >= height) {
        break;
      }

      if (graph[y][x] == TREE) {
        ++tree_count;
      }

      y += slope[index].y;

      x += slope[index].x;

      if (x >= width) {
        x = x - width;
      }
    }

    if (tree_count != 0) {
      tree_result *= tree_count;
    }
  }

  fprintf(stdout, "%ld\n", tree_result);

  return EXIT_SUCCESS;
}
