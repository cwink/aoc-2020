#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>

#define VECTOR_MAX_SIZE 4096

#define YEAR 2020

int main(void) {
  size_t vector_size = 0;

  int vector[VECTOR_MAX_SIZE];

  size_t i = 0;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  while (fscanf(stdin, "%d\n", &vector[vector_size]) != EOF) {
    ++vector_size;
  }

  for (; i < vector_size; ++i) {
    size_t j = 0;

    for (; j < vector_size; ++j) {
      size_t k = 0;

      for (; k < vector_size; ++k) {
        if (vector[i] + vector[j] + vector[k] == YEAR) {
          fprintf(stdout, "%d\n", vector[i] * vector[j] * vector[k]);

          return EXIT_SUCCESS;
        }
      }
    }
  }

  return EXIT_FAILURE;
}
