#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>

#define HASH_TABLE_MAX 1024

#define PAIR_MAX 64

#define YEAR 2020

typedef struct pair {
  int key;

  int value;
} PAIR;

static void hash_table_init(PAIR hash_table[HASH_TABLE_MAX][PAIR_MAX]) {
  size_t y = 0;

  for (; y < HASH_TABLE_MAX; ++y) {
    size_t x = 0;

    for (; x < PAIR_MAX; ++x) {
      hash_table[y][x].key = -1;

      hash_table[y][x].value = -1;
    }
  }
}

static int hash_table_insert(PAIR hash_table[HASH_TABLE_MAX][PAIR_MAX], int key, int value) {
  size_t x = 0;

  size_t hash = key % HASH_TABLE_MAX;

  for (; x < PAIR_MAX; ++x) {
    if (hash_table[hash][x].key == -1) {
      hash_table[hash][x].key = key;

      hash_table[hash][x].value = value;

      return 0;
    }
  }

  return -1;
}

static int hash_table_get(PAIR hash_table[HASH_TABLE_MAX][PAIR_MAX], int key) {
  size_t x = 0;

  size_t hash = key % HASH_TABLE_MAX;

  for (; x < PAIR_MAX; ++x) {
    if (hash_table[hash][x].key == key) {
      return hash_table[hash][x].value;
    }
  }

  return -1;
}

int main(void) {
  int value = 0;

  PAIR hash_table[HASH_TABLE_MAX][PAIR_MAX];

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  hash_table_init(hash_table);

  while (fscanf(stdin, "%d\n", &value) != EOF) {
    int key = hash_table_get(hash_table, value);

    if (key != -1) {
      fprintf(stdout, "%d\n", key * value);

      return EXIT_SUCCESS;
    }

    if (hash_table_insert(hash_table, YEAR - value, value) == -1) {
      fprintf(stderr, "ERROR: Unable to insert into hash table.");

      return EXIT_FAILURE;
    }
  }

  return EXIT_FAILURE;
}
