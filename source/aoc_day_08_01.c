#include "../include/aoc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 8

#define INSTRUCTIONS_SIZE 2048

#define HISTORY_SIZE 2048

typedef enum instruction_type_t { INSTRUCTION_TYPE_ACC, INSTRUCTION_TYPE_JMP, INSTRUCTION_TYPE_NOP, INSTRUCTION_TYPE_NONE } INSTRUCTION_TYPE;

typedef struct instruction_t {
  INSTRUCTION_TYPE type;

  int argument;
} INSTRUCTION;

typedef struct machine_t {
  INSTRUCTION instructions[INSTRUCTIONS_SIZE];

  int history[HISTORY_SIZE];

  size_t instructions_size;

  size_t history_size;

  int accumulator;

  size_t pc;
} MACHINE;

static void machine_init(MACHINE *machine) {
  size_t index = 0;

  for (; index < INSTRUCTIONS_SIZE; ++index) {
    machine->instructions[index].type = INSTRUCTION_TYPE_NONE;

    machine->instructions[index].argument = 0;
  }

  for (index = 0; index < HISTORY_SIZE; ++index) {
    machine->history[index] = -1;
  }

  machine->instructions_size = 0;

  machine->history_size = 0;

  machine->accumulator = 0;

  machine->pc = 0;
}

static void machine_instruction_add(MACHINE *machine, const char *const type, const int argument) {
  if (strcmp(type, "acc") == 0) {
    machine->instructions[machine->instructions_size].type = INSTRUCTION_TYPE_ACC;
  } else if (strcmp(type, "jmp") == 0) {
    machine->instructions[machine->instructions_size].type = INSTRUCTION_TYPE_JMP;
  } else {
    machine->instructions[machine->instructions_size].type = INSTRUCTION_TYPE_NOP;
  }

  machine->instructions[machine->instructions_size].argument = argument;

  ++machine->instructions_size;
}

static void machine_history_add(MACHINE *machine, const int offset) {
  machine->history[machine->history_size] = offset;

  ++machine->history_size;
}

static int machine_history_check(MACHINE *machine, const int offset) {
  size_t index = 0;

  for (; index < machine->history_size; ++index) {
    if (machine->history[index] == offset) {
      return 1;
    }
  }

  return 0;
}

static int machine_process(MACHINE *machine) {
  switch (machine->instructions[machine->pc].type) {
  case INSTRUCTION_TYPE_ACC:
    machine_history_add(machine, machine->pc);

    machine->accumulator += machine->instructions[machine->pc].argument;

    ++machine->pc;

    return 1;
  case INSTRUCTION_TYPE_JMP:
    machine_history_add(machine, machine->pc);

    machine->pc += machine->instructions[machine->pc].argument;

    return 1;
  case INSTRUCTION_TYPE_NOP:
    machine_history_add(machine, machine->pc);

    ++machine->pc;

    return 1;
  default:
    break;
  }

  return 0;
}

int main(void) {
  char buffer[BUFFER_SIZE];

  int argument;

  MACHINE machine;

  if (aoc_is_stdin_valid() != 1) {
    fprintf(stderr, "ERROR: Failed to read from stdin.\n");

    return EXIT_FAILURE;
  }

  machine_init(&machine);

  while (fscanf(stdin, "%s %d\n", buffer, &argument) != EOF) {
    machine_instruction_add(&machine, buffer, argument);
  }

  for (;;) {
    if (machine_history_check(&machine, machine.pc) == 1) {
      fprintf(stdout, "%d\n", machine.accumulator);

      break;
    }

    machine_process(&machine);
  }

  return EXIT_SUCCESS;
}
