find_program(CW_CLANG_FORMAT NAMES clang-format)

if(CW_CLANG_FORMAT)
  file(GLOB_RECURSE CW_ALL_SOURCES *.c *.h)

  add_custom_target(format COMMAND ${CW_CLANG_FORMAT} -style=file -i
                                   ${CW_ALL_SOURCES})
endif()
