add_custom_target(run)

foreach(CW_SOURCE_FILE ${CW_SOURCE_FILES})
  get_filename_component(CW_SOURCE_FILE_NOEXT ${CW_SOURCE_FILE} NAME_WE)

  add_custom_command(TARGET run COMMAND echo "=== ${CW_SOURCE_FILE_NOEXT}.x")

  add_custom_command(TARGET run COMMAND bash -c 'time ./${CW_SOURCE_FILE_NOEXT}.x<../data/${CW_SOURCE_FILE_NOEXT}.dat')

  add_custom_command(TARGET run COMMAND echo "")
endforeach()
