#ifndef AOC_H
#define AOC_H

#define AOC_ABS(N) ((N) < 0 ? -1 * (N) : (N))

#define AOC_FABS(N) ((N) < 0.0F ? -1.0F * (N) : (N))

int aoc_is_stdin_valid(void);

int aoc_round(const float value);

#endif /* AOC_H */
